import std.collection.*
/*
https://leetcode.cn/problems/TVdhkn/description/
剑指 Offer II 079. 所有子集，幂集
给定一个整数数组 nums ，数组中的元素 互不相同 。返回该数组所有可能的子集（幂集）。
解集 不能 包含重复的子集。你可以按 任意顺序 返回解集。
 */
main() {
    let nums: Array<Int> = [1, 2, 3]
    let lss = subsets(nums)
    let lss1 = subsets1(nums)
    let lss2 = subsets2(nums)
    println(lss)
    println(lss1)
    println(lss2)
}
/*
迭代法+二进制编码
问题转换：遍历n位2进制数的所有情况并输出对应位置的原数字
将每种情况编码到二进制表示中，0代表不选择，1代表选择
 */
func subsets(nums: Array<Int>): ArrayList<ArrayList<Int>> {
    let lss = ArrayList<ArrayList<Int>>()
    let ls = ArrayList<Int>()

    let sum = 1 << nums.size // 共有多少种选择情况
    for (i in 0..sum) { // 遍历每一种情况，获取二进制编码代表的原数字
        ls.clear()
        for (j in 0..nums.size) { // 获取i的每一位，bit=1 选择对应位的值，bit=0 不选择对应位的值
            if ((i & (1 << j)) != 0) {
                ls.append(nums[j])
            }
        }
        lss.append(ArrayList<Int>(ls))
    }

    return lss
}

// 回溯法
func subsets1(nums: Array<Int>): ArrayList<ArrayList<Int>> {
    let lss = ArrayList<ArrayList<Int>>()
    let ls = ArrayList<Int>()
    dfs(nums, 0, ls, lss) // 从nums第0个元素开始
    return lss
}

func dfs(nums: Array<Int>, i: Int, ls: ArrayList<Int>, lss: ArrayList<ArrayList<Int>>): Unit {
    if (i == nums.size) {
        lss.append(ArrayList<Int>(ls))
        return
    }

    ls.append(nums[i]) // 选择第i个数
    dfs(nums, i + 1, ls, lss) // 处理下1个数
    ls.remove(ls.size - 1) // 不选择第i个数
    dfs(nums, i + 1, ls, lss) // 处理下1个数
}

/*
增量蛮力法：把新数字增加到原来集合的所有幂集中+原来集合中的所有幂集=新的集合
 {{}}
 {{},{1}}
 {{},{1},{2},{1,2}}
 {{},{1},{2},{1,2},{3},{1,3},{2,3},{1,2,3}}
 */
func subsets2(nums: Array<Int>): ArrayList<ArrayList<Int>> {
    let lss = ArrayList<ArrayList<Int>>()
    let ls = ArrayList<Int>()
    lss.append(ArrayList<Int>()) // 初始添加空集
    f(nums, 0, lss) // 处理nums第0个元素
    return lss
}

// 处理nums第i个数
func f(nums: Array<Int>, i: Int, lss: ArrayList<ArrayList<Int>>): Unit {
    if (i < nums.size) {
        g(nums[i], lss)
        f(nums, i + 1, lss)
    }
}

// 增量插入元素
func g(e: Int, lss: ArrayList<ArrayList<Int>>): Unit {
    let lss2 = ArrayList<ArrayList<Int>>()

    // 将原集合每个元素集合添加新元素
    for (ls in lss) {
        let temps = ArrayList<Int>(ls)
        temps.append(e)
        lss2.append(temps)
    }

    // 将插入了新元素的集合并入原集合
    lss.appendAll(lss2)
}
