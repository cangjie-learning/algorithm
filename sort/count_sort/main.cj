import std.sort.*
import std.random.*
import std.collection.*
/*
计数排序是一种线性时间复杂度的排序算法，它特别适用于已知数据范围不是很大的场景。
计数排序的主要优点是它在数据范围较小时非常高效，特别是对于大数据集。然而，如果数据范围 (k) 非常大，计数排序可能不是最有效的选择，因为它需要的空间会随着 (k) 的增加而增加。此外，计数排序不是原地排序算法，因为它需要额外的存储空间来执行排序。

1. 时间复杂度：最好、平均和最坏情况下的时间复杂度都是 (O(n + k))，其中 (n) 是数组的长度，而 (k) 是数据的范围（即最大值和最小值的差）。在极端情况下，如果 (k) 接近 (n)，计数排序的时间复杂度可以退化为 (O(2n))，即 (O(n))。
2. 空间复杂度：计数排序的空间复杂度是 (O(k))，其中 (k) 是数据范围的大小。这是因为算法需要一个大小为 (k) 的额外数组来存储每个元素的计数。
3. 稳定性：计数排序是一种稳定的排序算法。它保持了相等元素的原始顺序。在计数排序中，每个元素根据其值被分配到一个特定的索引位置，如果多个元素具有相同的值，它们将被分配到连续的索引位置，从而保持了它们的相对顺序。
*/
main() {
    let times = 10000
    let maxLen = 100
    let maxVal = 100
    for (_ in 0..times) {
        let arr = getArr(maxLen, maxVal)
        let arr1 = Array<Int>(arr.size, item: 0)
        arr.copyTo(arr1, 0, 0, arr.size)
        cp(arr)
        countSort(arr1)
        if (arr != arr1) { // 数组直接比较
            throw Exception("err")
        }
    }
}

func getArr(maxLen: Int, maxVal: Int): Array<Int> {
    let random = Random()
    let len = random.nextInt64(maxLen)
    let arr = Array<Int>(len, {_ => random.nextInt64(maxVal)})
    return arr
}

// 对数器
func cp(arr: Array<Int>) {
    arr.sort()
}

// 利用数组下标有序性进行排序
func countSort(arr: Array<Int>) {
    if(arr.size==0){
        return
    }

    let max = max<Int>(arr).getOrThrow()
    let min = min<Int>(arr).getOrThrow()

    let cnts = Array<Int>((max - min + 1), item: 0)

    // 计数
    for (a in arr) {
        cnts[a - min]++
    }

    // cnts[值]=数量
    var j = 0
    for (i in 0..cnts.size) {
        while (cnts[i] > 0) {
            arr[j] = i + min
            j++
            cnts[i]--
        }
    }
}
