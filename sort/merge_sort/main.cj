import std.sort.*
import std.random.*

/*
归并排序是一种经典的排序算法。
归并排序的主要优点是其稳定性和对于大数据集的良好性能，特别是当数据不适合原地排序时。然而，由于它需要额外的存储空间，因此在空间受限的情况下可能不是最佳选择。此外，归并排序的实现相对复杂，特别是在处理链表而不是数组时。

1. 时间复杂度：无论最好、最坏还是平均情况，归并排序的时间复杂度都是 (O(n log n))，其中 (n) 是数组的长度。这是因为归并排序需要将数组分成两半，然后递归地排序这两半，最后将排序好的两半合并，每一步都需要 (O(n)) 的时间，而递归的深度是 (O(log n))。
2. 空间复杂度：归并排序的空间复杂度是 (O(n))，因为它需要一个额外的数组来存储合并过程中的元素。在合并两个已排序的子数组时，需要将它们的内容复制到一个临时数组中，然后再复制回原数组。
3. 稳定性：归并排序是一种稳定的排序算法。稳定性指的是排序算法在排序过程中，相等的元素之间的相对顺序是否保持不变。在归并排序中，当两个已排序的子数组合并时，如果存在相等的元素，它们可以保持原有的顺序。
*/
main() {
    let times = 10000
    let maxLen = 100
    let maxVal = 100
    for (_ in 0..times) {
        let arr = getArr(maxLen, maxVal)
        let arr1 = Array<Int>(arr.size, item: 0)
        arr.copyTo(arr1, 0, 0, arr.size)
        cp(arr)
        mergeSort(arr1)
        if (arr != arr1) { // 数组直接比较
            throw Exception("err")
        }
    }
}

func getArr(maxLen: Int, maxVal: Int): Array<Int> {
    let random = Random()
    let len = random.nextInt64(maxLen)
    let arr = Array<Int>(len, {_ => random.nextInt64(maxVal)})
    return arr
}

// 对数器
func cp(arr: Array<Int>) {
    arr.sort()
}

func swap(arr: Array<Int>, i: Int, j: Int) {
    let tmp = arr[i]
    arr[i] = arr[j]
    arr[j] = tmp
}

func mergeSort(arr: Array<Int>) {
    f(arr, 0, arr.size - 1)
}

//递归控制子序列个数和长度，再merge
func f(arr: Array<Int>, start: Int, end: Int): Unit {
    if (start < end) {
        let mid = start + (end - start) / 2
        f(arr, start, mid)
        f(arr, mid + 1, end)
        merge(arr, start, mid, end)
    }
}

// 比较两个子序列中的元素，将小的元素放入目标数组，将为剩余元素放入目标数组，最后将目标数组拷贝回原数组
func merge(arr: Array<Int>, start: Int, mid: Int, end: Int) {
    var i = start
    var j = mid + 1
    var k = 0
    let arr1 = Array<Int>((end - start + 1), item: 0)

    while (i <= mid && j <= end) {
        if (arr[i] < arr[j]) {
            arr1[k] = arr[i]
            i++
        } else {
            arr1[k] = arr[j]
            j++
        }
        k++
    }

    while (i <= mid) {
        arr1[k] = arr[i]
        i++
        k++
    }

    while (j <= end) {
        arr1[k] = arr[j]
        j++
        k++
    }

    arr1.copyTo(arr, 0, start, arr1.size)
}
