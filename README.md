|Dir|Intro|
|---|---|
|001_sort|排序算法|

# 准备环境 Windows
1. 申请 Cangjie 内测资格
2. 安装包 Cangjie-win-xxx.exe 
3. 插件 vscode Cangjie
```
# 插件配置
CJNative Backend = Path to Cangjie
```

# 经验
```txt
1. Option<T>, ?. 外层套Option<Option<T>>, 左值用 ?. 或 ??, 右值用 getOrThow()
```